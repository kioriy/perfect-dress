#  Creado con Kata Kuntur - Modelador de Datos
#  Versi�n: 2.5.2
#  Sitio Web: http://katakuntur.jeanmazuelos.com/
#  Si usted encuentra alg�n error le agradeceriamos lo reporte en:
#  http://pm.jeanmazuelos.com/katakuntur/issues/new

#  Administrador de Base de Datos: MySQL/MariaDB
#  Diagrama: dbperfectdress
#  Autor: ikioriy
#  Fecha y hora: 17/03/2019 13:41:47

# GENERANDO TABLAS
CREATE TABLE `VENTA` (
	`id_venta` INTEGER NOT NULL,
	`fecha` DATETIME NOT NULL,
	`fecha_entrega` DATETIME NOT NULL,
	PRIMARY KEY(`id_venta`)
) ENGINE=INNODB;
CREATE TABLE `PRODUCTO` (
	`id_producto` INTEGER NOT NULL,
	`modelo` VARCHAR(7) NOT NULL,
	`precio` DECIMAL(4,2) NOT NULL,
	`descripcion` VARCHAR(100) NOT NULL,
	PRIMARY KEY(`id_producto`)
) ENGINE=INNODB;
CREATE TABLE `CLIENTE` (
	`id_cliente` INTEGER NOT NULL,
	`nombre` VARCHAR(50) NOT NULL,
	`marca` VARCHAR(30) NOT NULL,
	`domicilio` VARCHAR(100) NOT NULL,
	`cp` VARCHAR(5) NOT NULL,
	`ciudad` VARCHAR(20) NOT NULL,
	`correo` VARCHAR(50) NOT NULL,
	`telefono_uno` VARCHAR(15) NOT NULL,
	`telefono_dos` VARCHAR(15) NOT NULL,
	PRIMARY KEY(`id_cliente`)
) ENGINE=INNODB;
CREATE TABLE `PROCESOS` (
	`id_procesos` INTEGER NOT NULL,
	`fecha` DATETIME NOT NULL,
	`tipo_pieza` VARCHAR(10) NOT NULL,
	`fk_id_empleado` INTEGER NOT NULL,
	KEY(`fk_id_empleado`),
	`fk_id_orden_venta` INTEGER NOT NULL,
	KEY(`fk_id_orden_venta`),
	PRIMARY KEY(`id_procesos`)
) ENGINE=INNODB;
CREATE TABLE `EMPLEADO` (
	`id_empleado` INTEGER NOT NULL,
	`nombre` VARCHAR(100) NOT NULL,
	`departamento` VARCHAR(15) NOT NULL,
	PRIMARY KEY(`id_empleado`)
) ENGINE=INNODB;
CREATE TABLE `MATERIA_PRIMA` (
	`id_materia_prima` INTEGER NOT NULL,
	`codigo` VARCHAR(15) NOT NULL,
	`tipo_materia_prima` VARCHAR(25) NOT NULL,
	`descripcion` VARCHAR(100) NOT NULL,
	`unidad_medida` VARCHAR(7) NOT NULL,
	`cantidad` DECIMAL(4,2) NOT NULL,
	`minimo_stock` DECIMAL(4,2) NOT NULL,
	PRIMARY KEY(`id_materia_prima`)
) ENGINE=INNODB;
CREATE TABLE `ORDEN_VENTA` (
	`id_orden_venta` INTEGER NOT NULL,
	`fecha` DATETIME NOT NULL,
	`fecha_entrega` DATETIME NOT NULL,
	`color_bordado` VARCHAR(15) NOT NULL,
	`color_vestido` VARCHAR(15) NOT NULL,
	`busto` DECIMAL(4,2) NOT NULL,
	`cintura` DECIMAL(4,2) NOT NULL,
	`cadera` DECIMAL(4,2) NOT NULL,
	`especificaciones_blusa` VARCHAR(900) NOT NULL,
	`tafeta` VARCHAR(15) NOT NULL,
	`tul` VARCHAR(15) NOT NULL,
	`glitter` VARCHAR(15) NOT NULL,
	`organza` VARCHAR(15) NOT NULL,
	`over` VARCHAR(15) NOT NULL,
	`cintura_falda` DECIMAL(4,2) NOT NULL,
	`largo_falda` DECIMAL(4,2) NOT NULL,
	`fk_id_producto` INTEGER NOT NULL,
	KEY(`fk_id_producto`),
	`fk_id_cliente` INTEGER NOT NULL,
	KEY(`fk_id_cliente`),
	PRIMARY KEY(`id_orden_venta`)
) ENGINE=INNODB;
CREATE TABLE `DETALLE_PRODUCCION` (
	`id_detalle_produccion` INTEGER NOT NULL,
	`cantidad` DECIMAL(4,2) NOT NULL,
	`fk_id_materia_prima` INTEGER NOT NULL,
	KEY(`fk_id_materia_prima`),
	`fk_id_producto` INTEGER NOT NULL,
	KEY(`fk_id_producto`),
	PRIMARY KEY(`id_detalle_produccion`)
) ENGINE=INNODB;

# GENERANDO RELACIONES
ALTER TABLE `PROCESOS` ADD CONSTRAINT `procesos_empleado_fk_id_empleado` FOREIGN KEY (`fk_id_empleado`) REFERENCES `EMPLEADO`(`id_empleado`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `PROCESOS` ADD CONSTRAINT `procesos_orden_venta_fk_id_orden_venta` FOREIGN KEY (`fk_id_orden_venta`) REFERENCES `ORDEN_VENTA`(`id_orden_venta`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `ORDEN_VENTA` ADD CONSTRAINT `orden_venta_producto_fk_id_producto` FOREIGN KEY (`fk_id_producto`) REFERENCES `PRODUCTO`(`id_producto`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `ORDEN_VENTA` ADD CONSTRAINT `orden_venta_cliente_fk_id_cliente` FOREIGN KEY (`fk_id_cliente`) REFERENCES `CLIENTE`(`id_cliente`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `DETALLE_PRODUCCION` ADD CONSTRAINT `detalle_produccion_materia_prima_fk_id_materia_prima` FOREIGN KEY (`fk_id_materia_prima`) REFERENCES `MATERIA_PRIMA`(`id_materia_prima`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `DETALLE_PRODUCCION` ADD CONSTRAINT `detalle_produccion_producto_fk_id_producto` FOREIGN KEY (`fk_id_producto`) REFERENCES `PRODUCTO`(`id_producto`) ON DELETE NO ACTION ON UPDATE CASCADE;