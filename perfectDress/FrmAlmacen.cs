﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace perfectDress
{
    public partial class FrmAlmacen : Form
    {
        MateriaPrima almacen = new MateriaPrima();
        bool nuevo = true;

        // NumberFormatInfo asociado con la cultura en-US.
        //NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;


        #region CONSTRUCTOR
        public FrmAlmacen()
        {
            InitializeComponent();
  
            ActionForm.placeHolder(this);
            this.ActiveControl = btnAceptar;

            //load();
        }

        public FrmAlmacen(MateriaPrima almacen)
        {
            InitializeComponent();

            this.almacen = almacen;

            loadForm();

            nuevo = false;
        }
        #endregion

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (nuevo)
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await almacen.insert(almacen.values()))
                    {
                        ActionForm.clear(this);
                        ActionForm.placeHolder(this);
                        lMensaje.Visible = false;
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
            else
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await almacen.update(almacen.values(), $"where id_materia_prima = {almacen.id_materia_prima.ToString()}"))
                    {
                        //ActionForm.clear(this);
                        this.Close();
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
        }

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        public void loadForm()
        {
            txtCodigo.Text = almacen.codigo; 
            txtTipo_materia_prima.Text = almacen.tipo_materia_prima;
            txtDescripcion.Text = almacen.descripcion;
            cmbUnidad_medida.Text = almacen.unidad_medida;
            txtCantidad.Text = almacen.cantidad.ToString();
            txtMinimo_stock.Text = almacen.minimo_stock.ToString();
        }

        public void loadClass()
        {
            almacen.codigo  = txtCodigo.Text;
            almacen.tipo_materia_prima = txtTipo_materia_prima.Text;
            almacen.descripcion = txtDescripcion.Text;
            almacen.unidad_medida = cmbUnidad_medida.Text;//txtUnidad_medida.Text;
            almacen.cantidad = Convert.ToDecimal(txtCantidad.Text);
            almacen.minimo_stock = Convert.ToDecimal(txtMinimo_stock.Text);
        }
    }
}
