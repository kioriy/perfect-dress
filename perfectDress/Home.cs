﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public partial class Home : Form
    {
        int menu_option = -1;
        int id = 1;

        Task<bool> t_respuesta;

        MateriaPrima almacen;
        Producto producto;
        Cliente cliente;
        

        ActionDataGridView action_data_grid_view;
        FrmOrdenVenta frm_orden_venta;
        FrmAlmacen frm_almacen;
        FrmCliente frm_cliente;
        FrmProducto frm_producto;

        public Home()
        {
            InitializeComponent();
        }

        #region BUTTON MENU
        private async void btnOrdenVenta_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnOrdenVenta.BackColor = Color.FromArgb(45, 45, 48);
            btnOrdenVenta.Font = new Font(btnOrdenVenta.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnOrdenVenta;
        }

        private async void btnAlmacen_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnAlmacen.BackColor = Color.FromArgb(45, 45, 48);
            btnAlmacen.Font = new Font(btnAlmacen.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnAlmacen;

            almacen = new MateriaPrima();
            t_respuesta = almacen.query("", "");

            action_data_grid_view = new ActionDataGridView(dgvGeneral);

            if (await t_respuesta)
            {
                action_data_grid_view.load(almacen.dataTable());
            }
        }

        private void btnProcesos_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnProcesos.BackColor = Color.FromArgb(45, 45, 48);
            btnProcesos.Font = new Font(btnProcesos.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnProcesos;
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnUsuarios.BackColor = Color.FromArgb(45, 45, 48);
            btnUsuarios.Font = new Font(btnUsuarios.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnUsuarios;
        }

        private async void btnCliente_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnCliente.BackColor = Color.FromArgb(45, 45, 48);
            btnCliente.Font = new Font(btnCliente.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnCliente;

            cliente = new Cliente();
            t_respuesta = cliente.query("", "");

            action_data_grid_view = new ActionDataGridView(dgvGeneral);

            if (await t_respuesta)
            {
                action_data_grid_view.load(cliente.dataTable());
            }
        }

        private async void btnProducto_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnProducto.BackColor = Color.FromArgb(45, 45, 48);
            btnProducto.Font = new Font(btnProducto.Font, FontStyle.Bold);
            menu_option = (int)menuPrincipal.btnProducto;

            producto = new Producto();
            t_respuesta = producto.query("", "");

            action_data_grid_view = new ActionDataGridView(dgvGeneral);

            if (await t_respuesta)
            {
                action_data_grid_view.load(producto.dataTable());
            }
        }
        #endregion

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            switch (menu_option)
            {
                case 0:
                    frm_orden_venta = new FrmOrdenVenta();
                    frm_orden_venta.Show();
                    break; 
                case 1:
                    frm_almacen = new FrmAlmacen();
                    frm_almacen.Show();
                    break;
                case 2:
                    //FrmAlmacen almacen = new FrmAlmacen();
                    //almacen.Show();
                    break;
                case 3:
                    //FrmAlmacen almacen = new FrmAlmacen();
                    //almacen.Show();
                    break;
                case 4:
                    frm_cliente = new FrmCliente();
                    frm_cliente.Show();
                    break;
                case 5:
                    frm_producto = new FrmProducto();
                    frm_producto.Show();
                    break;

                default:
                    btnOrdenVenta.Font = new Font(btnAlmacen.Font, FontStyle.Bold);
                    FrmInicioSesion inicioSesion = new FrmInicioSesion();
                    inicioSesion.Show();
                    break;
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (dgvGeneral.Rows != null)// & almacen != null)
            {
                if (id == -1)
                {
                    id = Convert.ToInt32(dgvGeneral.Rows[0].Cells[0].Value.ToString());
                }

                switch (menu_option)
                {
                    case 0:
                        //FrmAlmacen almacen = new FrmAlmacen();
                        //almacen.Show();
                        break;
                    case 1:
                        frm_almacen = new FrmAlmacen(almacen.list<MateriaPrima>().Find(x => x.id_materia_prima == id));
                        frm_almacen.Show();
                        break;
                    case 2:
                        //FrmAlmacen almacen = new FrmAlmacen();
                        //almacen.Show();
                        break;
                    case 3:
                        //FrmAlmacen almacen = new FrmAlmacen();
                        //almacen.Show();
                        break;
                    case 4:
                        frm_cliente = new FrmCliente(cliente.list<Cliente>().Find(x => x.id_cliente == id));
                        frm_cliente.Show();
                        break;
                    case 5:
                        frm_producto = new FrmProducto(producto.list<Producto>().Find(x => x.id_producto == id));
                        frm_producto.Show();
                        break;

                    default:
                        btnOrdenVenta.Font = new Font(btnAlmacen.Font, FontStyle.Bold);
                        FrmInicioSesion inicioSesion = new FrmInicioSesion();
                        inicioSesion.Show();
                        break;
                }  
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dgvGeneral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            id = Convert.ToInt32(this.dgvGeneral.CurrentRow.Cells[0].Value.ToString());
        }
    }
}
