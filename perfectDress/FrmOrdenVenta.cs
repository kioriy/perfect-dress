﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public partial class FrmOrdenVenta : Form
    {
        DetalleProduccion detalle_produccion = new DetalleProduccion();
        OrdenVenta orden_venta = new OrdenVenta();
        Producto producto = new Producto();
        Cliente cliente = new Cliente();

        bool nuevo = true;
        bool entra = true;

        int last_id_orden_venta = 0;


        #region CONSTRUCTOR
        public FrmOrdenVenta()
        {
            InitializeComponent();

            entra = false;
            cliente.loadComboBox(cmbCliente, "nombre", "id_cliente");
            producto.loadComboBox(cmbModelo, "modelo", "id_producto");
            entra = true;

            last_id_orden_venta = orden_venta.lastId().Result;

            ++last_id_orden_venta;

            ActionForm.placeHolder(this);
            txtFolio.Text = last_id_orden_venta.ToString();
            this.ActiveControl = btnAceptar;
        }

        public FrmOrdenVenta(OrdenVenta orden_venta)
        {
            InitializeComponent();

            this.orden_venta = orden_venta;

            loadForm();

            nuevo = false;
        }
        #endregion

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (nuevo)
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await orden_venta.insert(orden_venta.values()))
                    {
                        ActionForm.clear(this);
                        ActionForm.placeHolder(this);
                        lMensaje.Visible = false;
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
            else
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await orden_venta.update(orden_venta.values(), $"where id_orden_venta = {orden_venta.id_orden_venta.ToString()}"))
                    {
                        //ActionForm.clear(this);
                        this.Close();
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region COMBOBOX
        private void cmbModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra)
            {
                if (!string.IsNullOrEmpty(cmbModelo.SelectedValue.ToString()))
                {
                    detalle_produccion.resumenDetalleMateriaPrima(Convert.ToInt32(cmbModelo.SelectedValue.ToString()));
                }
            }
        }
        #endregion

        public void loadFormColores()
        {
            txtColor_bordado.Text = orden_venta.color_bordado;
            txtColor_vestido.Text = orden_venta.color_vestido;

            txtTafeta.Text = orden_venta.tafeta;
            txtTul.Text = orden_venta.tul;
            txtGlitter.Text = orden_venta.glitter;
            txtOrganza.Text = orden_venta.organza;
            txtCintura_falda.Text = orden_venta.cintura_falda.ToString();
            txtLargo_falda.Text = orden_venta.largo_falda.ToString();
            txtOver.Text = orden_venta.over;
            txtEspecificaciones_Falda.Text = orden_venta.especificaciones_falda;
        }

        public void loadForm()
        {
            //txtId_orden_venta.Text = orden_venta.id_cliente.ToString();
            cmbCliente.Text = orden_venta.fk_id_cliente.ToString();
            cmbModelo.Text = orden_venta.fk_id_producto.ToString();
            txtColor_bordado.Text = orden_venta.color_bordado;
            txtColor_vestido.Text = orden_venta.color_vestido;
            txtBusto.Text = orden_venta.busto.ToString();
            txtCintura.Text = orden_venta.cintura.ToString();
            txtCadera.Text = orden_venta.cadera.ToString();
            txtEspecificaciones_blusa.Text = orden_venta.especificaciones_blusa;

            txtTafeta.Text = orden_venta.tafeta;
            txtTul.Text = orden_venta.tul;
            txtGlitter.Text = orden_venta.glitter;
            txtOrganza.Text = orden_venta.organza;
            txtCintura_falda.Text = orden_venta.cintura_falda.ToString();
            txtLargo_falda.Text = orden_venta.largo_falda.ToString();
            txtOver.Text = orden_venta.over;
            txtEspecificaciones_Falda.Text = orden_venta.especificaciones_falda; 
        }

        public void loadClass()
        {
            orden_venta.fecha = DateTime.Today.ToString();
            orden_venta.fecha_entrega = dtpFecha_entrega.Value.ToString();
           
            orden_venta.color_bordado = txtColor_bordado.Text;
            orden_venta.color_vestido = txtColor_vestido.Text;
            orden_venta.busto = Convert.ToDecimal(txtBusto.Text);
            orden_venta.cintura= Convert.ToDecimal(txtCintura.Text);
            orden_venta.cadera = Convert.ToDecimal(txtCadera.Text);
            orden_venta.especificaciones_blusa = txtEspecificaciones_blusa.Text;

            orden_venta.tafeta = txtTafeta.Text;
            orden_venta.tul = txtTul.Text;
            orden_venta.glitter = txtGlitter.Text;
            orden_venta.organza = txtOrganza.Text;
            orden_venta.over = txtOver.Text;
            orden_venta.cintura_falda = Convert.ToDecimal(txtCintura_falda.Text);
            orden_venta.largo_falda = Convert.ToDecimal(txtLargo_falda.Text);
            orden_venta.especificaciones_falda = txtEspecificaciones_Falda.Text;

            orden_venta.tutu = chkTutu.Checked;

            orden_venta.estatus = estatus.Activa.ToString();

            orden_venta.fk_id_cliente = Convert.ToInt32(cmbCliente.SelectedValue.ToString());
            orden_venta.fk_id_producto = Convert.ToInt32(cmbModelo.SelectedValue.ToString());
        }
    }
}
