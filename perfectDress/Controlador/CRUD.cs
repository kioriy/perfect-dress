﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public class CRUD : WsC
    {
        public string table { get; set; }
        public string _values { get; set; }

        public string abrir_parentesis = "(";
        public string cerrar_parentesis = ")";
        public string c = "'";
        public string cc = ",";
        public string ccc = "','";

        public NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

        public async Task<bool> insert(string values)
        {
            return await executeAsync(table, values, action.insert.ToString(), "");
        }

        public async Task<bool> query(string values,  string where)
        {
            return await executeAsync(table, values, action.query.ToString(), where);
        }

        public async Task<bool> update(string values, string where)
        {
            return await executeAsync(table, values, action.update.ToString(), where);
        }

        public async Task<bool> delete(string where)
        {
            return await executeAsync(table, "", action.delete.ToString(), where);
        }

        public async Task<bool> free(string query)
        {
            return await executeAsync(table, "", action.free.ToString(), query);
        }

        public string implode(List<string> valores)
        {
            int items = valores.Count();
            string cadena = "";

            for (int i = 0; i < items; i++)
            {
                cadena += valores[i];

                if (i < (items - 1))
                {
                    cadena += "|";
                }
            }
            return cadena;
        }

        /*public void loadComboBox(ComboBox cb, List<string> _list, string display_member, string value_member)
        {
            //cb.Items.Clear();

            //query("1", $"WHERE ? ORDER BY `id_user` ASC");
            cb.DataSource = null;

            cb.DataSource = _list;
            cb.DisplayMember = display_member;
            cb.ValueMember = value_member;

        }*/
    }
}
