﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public class MateriaPrima : CRUD
    {
        public int id_materia_prima { get; set; }
        public string codigo { get; set; }
        public string tipo_materia_prima { get; set; }
        public string descripcion { get; set; }
        public string unidad_medida { get; set; }
        public decimal cantidad { get; set; }
        public decimal minimo_stock { get; set; }
        //public string _values { get; set; }
        //public string table { get; set; }
        
        public MateriaPrima()
        {
            //_values = values();
            table = "MATERIA_PRIMA";
        }

        public async void loadComboBox(ComboBox cb, string display_member, string value_member)
        {
            cb.Items.Clear();

            await query("1", $"WHERE ? ORDER BY `id_materia_prima` ASC");

            cb.DataSource = null;

            cb.DataSource = list<MateriaPrima>();
            cb.DisplayMember = display_member;
            cb.ValueMember = value_member;

            cb.SelectedIndex = -1;
        }

        public string values()
        {
            return
            $"{codigo}{cc}" +
            $"{tipo_materia_prima.ToUpper().Trim()}{cc}" +
            $"{descripcion.ToUpper().Trim()}{cc}" +
            $"{unidad_medida}{cc}" +
            $"{cantidad.ToString("N2", nfi)}{cc}" +
            $"{minimo_stock.ToString("N2", nfi)}";
        }
    }
}
