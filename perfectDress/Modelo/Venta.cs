﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perfectDress
{
    public class Venta : CRUD
    {
        public int id_venta { get; set; }
        public string fecha { get; set; }
        public string fecha_entrega { get; set; }
        public int fk_id_cliente { get; set; }
        public int fk_id_producto { get; set; }

        public Venta()
        {
            //_values = values();
            table = "VENTA";
        }
        public string values()
        {
            return
            $"{fecha.ToUpper().Trim()}{cc}" +
            $"{fecha_entrega.ToUpper().Trim()}{cc}" +
            $"{fk_id_cliente}{cc}" +
            $"{fk_id_producto}";
        }
    }
}
