﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perfectDress
{
    public class OrdenVenta : CRUD 
    {
        public int id_orden_venta { get; set; }
        public string fecha { get; set; }
        public string fecha_entrega { get; set; }
        public string color_bordado { get; set; }
        public string color_vestido { get; set; }
        public decimal busto { get; set; }
        public decimal cintura { get; set; }
        public decimal cadera { get; set; }
        public string especificaciones_blusa { get; set; }
        public string tafeta { get; set; }
        public string tul { get; set; }
        public string glitter { get; set; }
        public string organza { get; set; }
        public string over { get; set; }
        public decimal cintura_falda { get; set; }
        public decimal largo_falda { get; set; }
        public string especificaciones_falda { get; set; }
        public bool tutu { get; set; }
        public string estatus { get; set; }
        public int fk_id_cliente { get; set; }
        public int fk_id_producto { get; set; }

        public OrdenVenta()
        {
            //_values = values();
            table = "ORDEN_VENTA";
        }

        public async Task<int> lastId()
        {
            await query("1", "WHERE ? ORDER BY `ORDEN_VENTA`.`id_orden_venta` DESC LIMIT 1");

            if(list<OrdenVenta>() != null)
            {
                return list<OrdenVenta>()[0].id_orden_venta;
            }
            else
            {
                return 0;
            }
        }

        public string values()
        {
            return
            $"{fecha}{cc}" +
            $"{fecha_entrega}{cc}" +
            $"{color_bordado}{cc}" +
            $"{color_vestido}{cc}" +
            $"{busto.ToString("N2", nfi)}{cc}" +
            $"{cintura.ToString("N2", nfi)}{cc}" +
            $"{cadera.ToString("N2", nfi)}{cc}" +
            $"{especificaciones_blusa}{cc}" +
            $"{tafeta}{cc}" +
            $"{tul}{cc}" +
            $"{glitter}{cc}" +
            $"{organza}{cc}" +
            $"{over}{cc}" +
            $"{cintura_falda.ToString("N2", nfi)}{cc}" +
            $"{largo_falda.ToString("N2", nfi)}{cc}" +
            $"{especificaciones_falda}{cc}" +
            $"{tutu}{cc}" +
            $"{fk_id_cliente}{cc}" +
            $"{fk_id_producto}";
        }
    }
}
