﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public class Cliente : CRUD
    {
        public int id_cliente { get; set; }
        public string nombre { get; set; }
        public string marca { get; set; }
        public string domicilio { get; set; }
        public string cp { get; set; }
        public string ciudad { get; set; }
        public string correo { get; set; }
        public string telefono_uno { get; set; }
        public string telefono_dos { get; set; }
        //public string _values { get; set; }
        //public string table { get; set; }

        public Cliente()
        {
            //_values = values();
            table = "CLIENTE";
        }

        public async void loadComboBox(ComboBox cb, string display_member, string value_member)
        {
            cb.Items.Clear();

            await query("1", $"WHERE ? ORDER BY `id_cliente` ASC");

            cb.DataSource = null;

            cb.DataSource = list<Cliente>();
            cb.DisplayMember = display_member;
            cb.ValueMember = value_member;

            cb.SelectedIndex = -1;

            /*foreach (var item in _list)
            {
                cb.Items.Add(item.nombre.ToUpper());
            }*/
        }

        public string values()
        {
            return
            $"{nombre.ToUpper().Trim()}{cc}" +
            $"{marca.ToUpper().Trim()}{cc}" +
            $"{domicilio.ToUpper().Trim()}{cc}" +
            $"{cp.ToUpper().Trim()}{cc}" +
            $"{ciudad.ToUpper().Trim()}{cc}" +
            $"{correo.Trim()}{cc}" +
            $"{telefono_uno.ToUpper().Trim()}{cc}" +
            $"{telefono_dos.ToUpper().Trim()}";
        }
    }
}
