﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public class Producto : CRUD
    {
        public int id_producto { get; set; }
        public string modelo { get; set; }
        public decimal precio { get; set; }
        public string descripcion { get; set; }
       
    
        public Producto()
        {
            table = "PRODUCTO";
        }

        public async void loadComboBox(ComboBox cb, string display_member, string value_member)
        {
            cb.Items.Clear();

            await query("1", $"WHERE ? ORDER BY `id_producto` ASC");

            cb.DataSource = null;

            cb.DataSource = list<Producto>();
            cb.DisplayMember = display_member;
            cb.ValueMember = value_member;

            cb.SelectedIndex = -1;
        }

        public async Task<int> lastId()
        {
            await query("1", "WHERE ? ORDER BY `PRODUCTO`.`id_producto` DESC LIMIT 1");

            if (list<Producto>() != null)
            {
                return list<Producto>()[0].id_producto;
            }
            else
            {
                return 0;
            }
        }

        public string values()
        {
            return
            $"{modelo}{cc}" +
            $"{precio}{cc}" +//$"{precio.ToString("N2", nfi)}{cc}" +
            $"{descripcion.ToUpper().Trim()}";
        }
    }
}
