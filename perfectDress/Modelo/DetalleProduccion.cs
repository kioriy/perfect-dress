﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    class DetalleProduccion : CRUD
    {
        public int id_detalle_produccion { get; set; }
        public string cantidad { get; set; }
        public decimal fk_id_materia_prima { get; set; }
        public string fk_id_producto { get; set; }


        public DetalleProduccion()
        {
            table = "DETALLE_PRODUCCION";
        }

        public async void loadComboBox(ComboBox cb, string display_member, string value_member)
        {
            cb.Items.Clear();

            await query("1", $"WHERE ? ORDER BY `id_detalle_produccion` ASC");

            cb.DataSource = null;

            cb.DataSource = list<Producto>();
            cb.DisplayMember = display_member;
            cb.ValueMember = value_member;

            cb.SelectedIndex = -1;
        }

        public async void resumenDetalleMateriaPrima(int id_producto)//ComboBox cb, string display_member, string value_member)
        {
            await free($"SELECT MP.id_materia_prima, DP.cantidad, MP.descripcion, MP.codigo, DP.id_detalle_produccion FROM DETALLE_PRODUCCION DP INNER JOIN MATERIA_PRIMA MP ON  DP.fk_id_materia_prima = MP.id_materia_prima WHERE DP.fk_id_producto = {id_producto}");
        }

        public string values()
        {
            return
            $"{cantidad}{cc}" +
            $"{fk_id_materia_prima.ToString()}{cc}" +
            $"{fk_id_producto.ToString()}";
        }
    }
}
