﻿namespace perfectDress
{
    partial class FrmOrdenVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrdenVenta));
            this.txtBusto = new System.Windows.Forms.TextBox();
            this.txtColor_vestido = new System.Windows.Forms.TextBox();
            this.txtEspecificaciones_blusa = new System.Windows.Forms.TextBox();
            this.txtCintura = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lMensaje = new System.Windows.Forms.Label();
            this.txtOver = new System.Windows.Forms.TextBox();
            this.txtColor_bordado = new System.Windows.Forms.TextBox();
            this.txtCadera = new System.Windows.Forms.TextBox();
            this.txtOrganza = new System.Windows.Forms.TextBox();
            this.cmbCliente = new System.Windows.Forms.ComboBox();
            this.dtpFecha_entrega = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbModelo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLargo_falda = new System.Windows.Forms.TextBox();
            this.txtCintura_falda = new System.Windows.Forms.TextBox();
            this.txtTul = new System.Windows.Forms.TextBox();
            this.txtTafeta = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGlitter = new System.Windows.Forms.TextBox();
            this.txtEspecificaciones_Falda = new System.Windows.Forms.TextBox();
            this.txtFolio = new System.Windows.Forms.TextBox();
            this.chkTutu = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBusto
            // 
            this.txtBusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtBusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBusto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBusto.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusto.Location = new System.Drawing.Point(148, 194);
            this.txtBusto.Name = "txtBusto";
            this.txtBusto.Size = new System.Drawing.Size(100, 29);
            this.txtBusto.TabIndex = 1;
            // 
            // txtColor_vestido
            // 
            this.txtColor_vestido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtColor_vestido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtColor_vestido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtColor_vestido.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColor_vestido.Location = new System.Drawing.Point(12, 253);
            this.txtColor_vestido.Name = "txtColor_vestido";
            this.txtColor_vestido.Size = new System.Drawing.Size(124, 29);
            this.txtColor_vestido.TabIndex = 2;
            // 
            // txtEspecificaciones_blusa
            // 
            this.txtEspecificaciones_blusa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtEspecificaciones_blusa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEspecificaciones_blusa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEspecificaciones_blusa.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEspecificaciones_blusa.Location = new System.Drawing.Point(148, 229);
            this.txtEspecificaciones_blusa.Multiline = true;
            this.txtEspecificaciones_blusa.Name = "txtEspecificaciones_blusa";
            this.txtEspecificaciones_blusa.Size = new System.Drawing.Size(312, 53);
            this.txtEspecificaciones_blusa.TabIndex = 4;
            // 
            // txtCintura
            // 
            this.txtCintura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtCintura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCintura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCintura.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCintura.Location = new System.Drawing.Point(254, 194);
            this.txtCintura.Name = "txtCintura";
            this.txtCintura.Size = new System.Drawing.Size(100, 29);
            this.txtCintura.TabIndex = 5;
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.btnAceptar.FlatAppearance.BorderSize = 0;
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptar.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnAceptar.Image = ((System.Drawing.Image)(resources.GetObject("btnAceptar.Image")));
            this.btnAceptar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAceptar.Location = new System.Drawing.Point(361, 486);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(99, 70);
            this.btnAceptar.TabIndex = 10;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAceptar.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 33);
            this.panel1.TabIndex = 13;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(439, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(33, 33);
            this.btnCerrar.TabIndex = 15;
            this.btnCerrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(155, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Orden de venta";
            // 
            // lMensaje
            // 
            this.lMensaje.AutoSize = true;
            this.lMensaje.Font = new System.Drawing.Font("Segoe UI Emoji", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMensaje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(90)))), ((int)(((byte)(74)))));
            this.lMensaje.Location = new System.Drawing.Point(12, 519);
            this.lMensaje.Name = "lMensaje";
            this.lMensaje.Size = new System.Drawing.Size(49, 17);
            this.lMensaje.TabIndex = 14;
            this.lMensaje.Text = "label2";
            this.lMensaje.Visible = false;
            // 
            // txtOver
            // 
            this.txtOver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtOver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOver.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOver.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOver.Location = new System.Drawing.Point(150, 391);
            this.txtOver.Name = "txtOver";
            this.txtOver.Size = new System.Drawing.Size(98, 29);
            this.txtOver.TabIndex = 6;
            // 
            // txtColor_bordado
            // 
            this.txtColor_bordado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtColor_bordado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtColor_bordado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtColor_bordado.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColor_bordado.Location = new System.Drawing.Point(12, 194);
            this.txtColor_bordado.Name = "txtColor_bordado";
            this.txtColor_bordado.Size = new System.Drawing.Size(124, 29);
            this.txtColor_bordado.TabIndex = 7;
            // 
            // txtCadera
            // 
            this.txtCadera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtCadera.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCadera.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCadera.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCadera.Location = new System.Drawing.Point(360, 194);
            this.txtCadera.Name = "txtCadera";
            this.txtCadera.Size = new System.Drawing.Size(100, 29);
            this.txtCadera.TabIndex = 8;
            // 
            // txtOrganza
            // 
            this.txtOrganza.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtOrganza.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOrganza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOrganza.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrganza.Location = new System.Drawing.Point(150, 332);
            this.txtOrganza.Name = "txtOrganza";
            this.txtOrganza.Size = new System.Drawing.Size(98, 29);
            this.txtOrganza.TabIndex = 9;
            // 
            // cmbCliente
            // 
            this.cmbCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.cmbCliente.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbCliente.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCliente.FormattingEnabled = true;
            this.cmbCliente.Items.AddRange(new object[] {
            "CAJA",
            "MTS",
            "KG",
            "PZA"});
            this.cmbCliente.Location = new System.Drawing.Point(12, 59);
            this.cmbCliente.Name = "cmbCliente";
            this.cmbCliente.Size = new System.Drawing.Size(319, 29);
            this.cmbCliente.TabIndex = 16;
            // 
            // dtpFecha_entrega
            // 
            this.dtpFecha_entrega.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha_entrega.Location = new System.Drawing.Point(12, 114);
            this.dtpFecha_entrega.Name = "dtpFecha_entrega";
            this.dtpFecha_entrega.Size = new System.Drawing.Size(319, 29);
            this.dtpFecha_entrega.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Fecha de entrega";
            // 
            // cmbModelo
            // 
            this.cmbModelo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.cmbModelo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbModelo.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbModelo.FormattingEnabled = true;
            this.cmbModelo.Location = new System.Drawing.Point(337, 114);
            this.cmbModelo.Name = "cmbModelo";
            this.cmbModelo.Size = new System.Drawing.Size(123, 29);
            this.cmbModelo.TabIndex = 20;
            this.cmbModelo.SelectedIndexChanged += new System.EventHandler(this.cmbModelo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(164, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "- DATOS BLUSA -";
            // 
            // txtLargo_falda
            // 
            this.txtLargo_falda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtLargo_falda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLargo_falda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLargo_falda.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLargo_falda.Location = new System.Drawing.Point(360, 332);
            this.txtLargo_falda.Name = "txtLargo_falda";
            this.txtLargo_falda.Size = new System.Drawing.Size(100, 29);
            this.txtLargo_falda.TabIndex = 25;
            // 
            // txtCintura_falda
            // 
            this.txtCintura_falda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtCintura_falda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCintura_falda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCintura_falda.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCintura_falda.Location = new System.Drawing.Point(254, 332);
            this.txtCintura_falda.Name = "txtCintura_falda";
            this.txtCintura_falda.Size = new System.Drawing.Size(100, 29);
            this.txtCintura_falda.TabIndex = 24;
            // 
            // txtTul
            // 
            this.txtTul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtTul.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTul.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTul.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTul.Location = new System.Drawing.Point(11, 391);
            this.txtTul.Name = "txtTul";
            this.txtTul.Size = new System.Drawing.Size(133, 29);
            this.txtTul.TabIndex = 23;
            // 
            // txtTafeta
            // 
            this.txtTafeta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtTafeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTafeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTafeta.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTafeta.Location = new System.Drawing.Point(11, 332);
            this.txtTafeta.Name = "txtTafeta";
            this.txtTafeta.Size = new System.Drawing.Size(133, 29);
            this.txtTafeta.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(164, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "- DATOS FALDA -";
            // 
            // txtGlitter
            // 
            this.txtGlitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtGlitter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGlitter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGlitter.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlitter.Location = new System.Drawing.Point(11, 450);
            this.txtGlitter.Name = "txtGlitter";
            this.txtGlitter.Size = new System.Drawing.Size(133, 29);
            this.txtGlitter.TabIndex = 27;
            // 
            // txtEspecificaciones_Falda
            // 
            this.txtEspecificaciones_Falda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtEspecificaciones_Falda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEspecificaciones_Falda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEspecificaciones_Falda.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEspecificaciones_Falda.Location = new System.Drawing.Point(148, 426);
            this.txtEspecificaciones_Falda.Multiline = true;
            this.txtEspecificaciones_Falda.Name = "txtEspecificaciones_Falda";
            this.txtEspecificaciones_Falda.Size = new System.Drawing.Size(312, 53);
            this.txtEspecificaciones_Falda.TabIndex = 28;
            // 
            // txtFolio
            // 
            this.txtFolio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.txtFolio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFolio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFolio.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolio.Location = new System.Drawing.Point(337, 59);
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(123, 29);
            this.txtFolio.TabIndex = 29;
            this.txtFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chkTutu
            // 
            this.chkTutu.AutoSize = true;
            this.chkTutu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTutu.Location = new System.Drawing.Point(264, 391);
            this.chkTutu.Name = "chkTutu";
            this.chkTutu.Size = new System.Drawing.Size(67, 25);
            this.chkTutu.TabIndex = 30;
            this.chkTutu.Text = "Tu Tu";
            this.chkTutu.UseVisualStyleBackColor = true;
            // 
            // FrmOrdenVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(472, 568);
            this.Controls.Add(this.chkTutu);
            this.Controls.Add(this.txtFolio);
            this.Controls.Add(this.txtEspecificaciones_Falda);
            this.Controls.Add(this.txtGlitter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLargo_falda);
            this.Controls.Add(this.txtCintura_falda);
            this.Controls.Add(this.txtTul);
            this.Controls.Add(this.txtTafeta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbModelo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpFecha_entrega);
            this.Controls.Add(this.cmbCliente);
            this.Controls.Add(this.txtOrganza);
            this.Controls.Add(this.txtCadera);
            this.Controls.Add(this.txtColor_bordado);
            this.Controls.Add(this.txtOver);
            this.Controls.Add(this.lMensaje);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.txtCintura);
            this.Controls.Add(this.txtEspecificaciones_blusa);
            this.Controls.Add(this.txtColor_vestido);
            this.Controls.Add(this.txtBusto);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmOrdenVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZZ";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtBusto;
        private System.Windows.Forms.TextBox txtColor_vestido;
        private System.Windows.Forms.TextBox txtEspecificaciones_blusa;
        private System.Windows.Forms.TextBox txtCintura;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Label lMensaje;
        private System.Windows.Forms.TextBox txtOver;
        private System.Windows.Forms.TextBox txtColor_bordado;
        private System.Windows.Forms.TextBox txtCadera;
        private System.Windows.Forms.TextBox txtOrganza;
        private System.Windows.Forms.ComboBox cmbCliente;
        private System.Windows.Forms.DateTimePicker dtpFecha_entrega;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbModelo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLargo_falda;
        private System.Windows.Forms.TextBox txtCintura_falda;
        private System.Windows.Forms.TextBox txtTul;
        private System.Windows.Forms.TextBox txtTafeta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGlitter;
        private System.Windows.Forms.TextBox txtEspecificaciones_Falda;
        private System.Windows.Forms.TextBox txtFolio;
        private System.Windows.Forms.CheckBox chkTutu;
    }
}