﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Data;

namespace perfectDress
{
    class ActionDataGridView
    {
        #region variables;
        private DataGridView dgv;
        #endregion

        #region constructor
        public ActionDataGridView(DataGridView dgv)
        {
            this.dgv = dgv;

            style();

            //dgv.CellContentClick += new DataGridViewCellEventHandler(idRow);
        }

        //public void idRow(object sender, DataGridViewCellEventArgs e)
        //{
            //string dato = dgv.CurrentRow.Cells[0].Value.ToString();
        //}
        #endregion

        #region metodos

        public void style()
        {
            dgv.BorderStyle = BorderStyle.None;
            /*dgv.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            dgv.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            dgv.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            dgv.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;*/

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dgv.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgv.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgv.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgv.BackgroundColor = Color.White;

            dgv.EnableHeadersVisualStyles = false;
            dgv.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        public void load(DataTable dt)
        {
            this.dgv.DataSource = dt;
        }

        public void cargarDgvMateriaPrima(MateriaPrima materia_prima, decimal cantidad)
        {
            int id_materia_prima = materia_prima.id_materia_prima;
            string codigo = materia_prima.codigo;
            string tipo_materia_prima = materia_prima.tipo_materia_prima;
            string descripcion = materia_prima.descripcion;
            string unidad_medida = materia_prima.unidad_medida;
            //decimal cantidad = cantidad; //materia_prima.cantidad;
            decimal minimo_stock = materia_prima.minimo_stock;

            dgv.Rows.Add(id_materia_prima, cantidad, descripcion, codigo, "");
        }

        public void cargarDgvMateriaPrima(DataTable dt_materia_prima)
        {
            foreach (DataRow row in dt_materia_prima.Rows)
            {
                dgv.Rows.Add(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString());
            }
        }

        public bool sumarProductoExistente(DataGridView dgv, string codigo)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["cCodigo"].Value.ToString() == codigo)
                {
                    decimal cantidad = Convert.ToDecimal(row.Cells["cCantidad"].Value);
                    decimal precio = Convert.ToDecimal(row.Cells["cPrecioUnitario"].Value);
                    decimal total = 0.0M;
                    cantidad++;
                    row.Cells["cCantidad"].Value = cantidad.ToString();
                    total = cantidad * precio;
                    row.Cells["cTotal"].Value = string.Format("{0:N2}", total);
                    return true;
                }
            }
            return false;
        }

        public string sumarTotalPagado(DataGridView dgv)
        {
            double suma = 0;

            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["cTotal"].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells["cTotal"].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            return string.Format("{0:N2}", suma);//suma.ToString("C"); 
        }

        public List<string> listaColumna(string columna)
        {
            List<string> lista_columna = new List<string>();

            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells[columna].Value == DBNull.Value)
                    continue;

                lista_columna.Add(string.Format("{0:N2}", row.Cells[columna].Value.ToString()));
                //double valorcell = 0;
                //double.TryParse(Convert.ToString(row.Cells["cTotal"].Value), out valorcell);
                //suma += Convert.ToDouble(valorcell);
            }
            return lista_columna;
        }

        public void sumarFilas(DataGridViewCellEventArgs e)
        {
            if (this.dgv.Columns[e.ColumnIndex].Name == "cCantidad")
            {
                decimal cantidad = Convert.ToDecimal(this.dgv.CurrentRow.Cells["cCantidad"].Value);
                decimal precioU = Convert.ToDecimal(this.dgv.CurrentRow.Cells["cPrecioUnitario"].Value);
                decimal total = cantidad * precioU;

                this.dgv.CurrentRow.Cells["cTotal"].Value = string.Format("{0:N2}", total);
            }
        }
        #endregion
    }
}
