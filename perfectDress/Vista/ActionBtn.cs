﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace perfectDress
{
    public static class ActionBtn
    {
        public static void fontStyleNormal(Form form)
        {
            foreach (Control control in form.Controls)
            {
                //if(control is Button)
                //{
                //    control.Font = new Font(control.Font, FontStyle.Regular);
                //}
                if (control is Panel & control.Name == "pPrincipal")
                {
                    foreach (Control controlBtn in control.Controls)
                    {
                        if(controlBtn is Button)
                        {
                            controlBtn.Font = new Font(controlBtn.Font, FontStyle.Regular);
                        }
                    }
                }
            }
        }

        public static void backColor(Form form)
        {
            foreach (Control control in form.Controls)
            {
                //if (control is Button)
                //{
                    //control.Font = new Font(control.Font, FontStyle.Regular);
                //}
                if (control is Panel & control.Name == "pPrincipal")
                {
                    foreach (Control controlBtn in control.Controls)
                    {
                        if (controlBtn is Button)
                        {
                            controlBtn.BackColor = Color.FromArgb(54, 70, 139);
                        }
                    }
                }
            }
        }
    }
}
