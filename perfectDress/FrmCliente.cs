﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public partial class FrmCliente : Form
    {
        Cliente cliente = new Cliente();
        bool nuevo = true;
        
        #region CONSTRUCTOR
        public FrmCliente()
        {
            InitializeComponent();

            ActionForm.placeHolder(this);
            this.ActiveControl = btnAceptar;

            //load();
        }

        public FrmCliente(Cliente cliente)
        {
            InitializeComponent();

            this.cliente = cliente;

            loadForm();

            nuevo = false;
        }
        #endregion

        private async void btnAceptar_Click_1(object sender, EventArgs e)
        {
            if (nuevo)
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await cliente.insert(cliente.values()))
                    {
                        ActionForm.clear(this);
                        ActionForm.placeHolder(this);
                        lMensaje.Visible = false;
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
            else
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClass();

                    if (await cliente.update(cliente.values(), $"where id_cliente = {cliente.id_cliente.ToString()}"))
                    {
                        //ActionForm.clear(this);
                        this.Close();
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        public void loadForm()
        {
            //txtId_Cliente.Text = cliente.id_cliente.ToString();
            txtNombre.Text = cliente.nombre;
            txtDomicilio.Text = cliente.domicilio;
            txtCp.Text = cliente.cp;
            txtCiudad.Text = cliente.ciudad.ToString();
            txtCorreo.Text = cliente.correo.ToString();
            txtMarca.Text = cliente.marca.ToString();
            txtTelefono_uno.Text = cliente.telefono_uno.ToString();
            txtTelefono_dos.Text = cliente.telefono_dos.ToString();
        }

        public void loadClass()
        {
            //cliente.id_cliente = Convert.ToInt32(txtId_Cliente.Text);
            cliente.nombre = txtNombre.Text;
            cliente.domicilio =  txtDomicilio.Text;
            cliente.cp = txtCp.Text;
            cliente.ciudad = txtCiudad.Text;
            cliente.correo = txtCorreo.Text;
            cliente.marca = txtMarca.Text;
            cliente.telefono_uno = txtTelefono_uno.Text;
            cliente.telefono_dos = txtTelefono_dos.Text;
        }
    }
}
