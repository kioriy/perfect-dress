﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perfectDress
{
    public partial class FrmProducto : Form
    {
        DetalleProduccion detalle_produccion = new DetalleProduccion();
        MateriaPrima materia_prima = new MateriaPrima();
        Producto producto = new Producto();

        List<string> list_values_update_detalle_produccion = new List<string>();
        List<string> list_where_update_detalle_produccion = new List<string>();
        List<string> list_values_detalle_produccion = new List<string>();
        List<MateriaPrima> list_materia_prima = new List<MateriaPrima>();


        ActionDataGridView dgv;

        int last_id_producto;
        int id_detalle_produccion;

        bool entra = true;
        bool nuevo = true;

        string values_detalle_produccion = "";
        string values_update_detalle_produccion = "";
        string values_where_update_detalle_produccion = "";

        #region CONSTRUCTOR
        public FrmProducto()
        {
            InitializeComponent();

            dgv = new ActionDataGridView(dgvMateriaPrima);
            entra = false;
            materia_prima.loadComboBox(cmbMateria_prima_false, "descripcion", "id_materia_prima");
            entra = true;
            ActionForm.placeHolder(this);
            this.ActiveControl = btnAceptar;
        }

        public FrmProducto(Producto producto)
        {
            InitializeComponent();

            entra = false;
            materia_prima.loadComboBox(cmbMateria_prima_false, "descripcion", "id_materia_prima");
            entra = true;

            this.producto = producto;

            loadFormProducto();
            detalle_produccion.resumenDetalleMateriaPrima(producto.id_producto);
 
            dgv = new ActionDataGridView(dgvMateriaPrima);
            dgv.cargarDgvMateriaPrima(detalle_produccion.dataTable());

            nuevo = false;
        }
        #endregion

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (nuevo)
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClassProducto();

                    if (await producto.insert(producto.values()))
                    {
                        last_id_producto = producto.lastId().Result;
                       
                        loadValuesDetalleProduccion();

                        if (await detalle_produccion.insert(values_detalle_produccion))
                        {
                            ActionForm.clear(this);
                            ActionForm.placeHolder(this);
                            lMensaje.Visible = false;
                            dgvMateriaPrima.Rows.Clear();
                        }
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
            else
            {
                if (!ActionForm.isEmpty(this))
                {
                    loadClassProducto();

                    if (await producto.update(producto.values(), $"where id_producto = {producto.id_producto}"))
                    {
                        last_id_producto = producto.id_producto;

                        loadValuesDetalleProduccion();

                        if (await detalle_produccion.update(values_update_detalle_produccion, values_where_update_detalle_produccion))
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        lMensaje.Visible = true;
                        lMensaje.Text = Mensaje.result;
                    }
                }
                else
                {
                    lMensaje.Visible = true;
                    lMensaje.Text = Mensaje.result;
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region COMBOBOX
        private void cmbMateria_prima_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra)
            {
                if (txtCantidad_false.ForeColor != Color.Gray)
                {
                    lMensaje.Visible = false;

                    //carga el dataGridView con los datos del comboBox
                    if (!string.IsNullOrEmpty(cmbMateria_prima_false.SelectedValue.ToString()))
                    {
                        dgv.cargarDgvMateriaPrima(materia_prima.list<MateriaPrima>().Find(x => x.id_materia_prima == Convert.ToInt32(cmbMateria_prima_false.SelectedValue.ToString())), Convert.ToDecimal(txtCantidad_false.Text));

                        cmbMateria_prima_false.Text = " ";
                        txtCantidad_false.Text = "";
                        txtCantidad_false.Focus();
                    }
                }
                else
                {
                    cmbMateria_prima_false.Text = " ";
                    lMensaje.Visible = true;
                    lMensaje.Text = "La cantidad es requerida";
                }
            }
        }
        #endregion

        #region DATAGRIDVIEW
        private async void dgvMateriaPrima_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            await detalle_produccion.delete($"WHERE id_detalle_produccion = {id_detalle_produccion}");
        }

        private void dgvMateriaPrima_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!string.IsNullOrEmpty(dgvMateriaPrima.CurrentRow.Cells["cIdDetalleProduccion"].Value.ToString()))
            {
                id_detalle_produccion = Convert.ToInt32(dgvMateriaPrima.CurrentRow.Cells["cIdDetalleProduccion"].Value.ToString());
            }
        }
        #endregion

        public void loadFormProducto()
        {
            txtModelo.Text = producto.modelo;
            txtPrecio.Text = producto.precio.ToString("N2", producto.nfi);
            txtDescripcion.Text = producto.descripcion;
        }

        public void loadClassProducto()
        {
            producto.modelo = txtModelo.Text;
            producto.precio = Convert.ToDecimal(txtPrecio.Text);
            producto.descripcion = txtDescripcion.Text;
        }

        public void loadFormDetalleProduccion()
        {
            //txtId_producto.Text = producto.id_producto.ToString();
            txtModelo.Text = producto.modelo;
            txtPrecio.Text = producto.precio.ToString();
            txtDescripcion.Text = producto.descripcion;
            /*txtCiudad.Text = producto.ciudad.ToString();"
            txtCorreo.Text = producto.correo.ToString(); '\ 
            txtMarca.Text = producto.marca.ToString();
            txtTelefono_uno.Text = producto.telefono_uno.ToString();
            txtTelefono_dos.Text = producto.telefono_dos.ToString();*/
        }

        public void loadValuesDetalleProduccion()
        {
            foreach (DataGridViewRow fila in dgvMateriaPrima.Rows)
            {
                if(string.IsNullOrEmpty(fila.Cells["cIdDetalleProduccion"].Value.ToString()))
                {
                    list_values_detalle_produccion.Add
                           (
                            $"{fila.Cells["cCantidad"].Value.ToString()}," +
                            $"{fila.Cells["cIdMateriaPrima"].Value.ToString()}," +
                            $"{last_id_producto}"
                           );
                }
                else
                {
                    if (!nuevo)
                    {
                        list_values_update_detalle_produccion.Add
                              (
                               $"{fila.Cells["cCantidad"].Value.ToString()}," +
                               $"{fila.Cells["cIdMateriaPrima"].Value.ToString()}," +
                               $"{last_id_producto}"
                              );

                        list_where_update_detalle_produccion.Add
                            (
                                $"WHERE id_detalle_produccion = {fila.Cells["cIdDetalleProduccion"].Value.ToString()}"
                            );
                    }
                }
            }
            if (list_values_detalle_produccion.Count > 1 | list_values_update_detalle_produccion.Count > 1)
            {
                if (list_values_detalle_produccion.Count != 0)
                {
                    values_detalle_produccion = detalle_produccion.implode(list_values_detalle_produccion);
                }

                if (!nuevo & list_values_update_detalle_produccion.Count != 0)
                {
                    values_update_detalle_produccion = detalle_produccion.implode(list_values_update_detalle_produccion);
                    values_where_update_detalle_produccion = detalle_produccion.implode(list_where_update_detalle_produccion);
                }
            }
            else
            {
                if (list_values_detalle_produccion.Count != 0)
                {
                    values_detalle_produccion = list_values_detalle_produccion[0];
                }

                if (!nuevo & list_values_update_detalle_produccion.Count != 0)
                {
                    values_update_detalle_produccion = list_values_update_detalle_produccion[0];
                    values_where_update_detalle_produccion = list_where_update_detalle_produccion[0];
                }
            }
        }
    }
}
